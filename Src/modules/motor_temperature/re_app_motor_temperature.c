/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_gear_status.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "motor_temperature/re_app_motor_temperature.h"

bool tx_thc_Flag = false;

/**
 * @Brief RE_Read_Thc_Status
 * this function checks for the thermal cutoff status
 * @Param None
 * @Retval Gear Direction
 */
uint8_t RE_Read_Thc_Status (void)
{
    return HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2);
}

/**
 * @Brief RE_TurnON_Thc
 * this function turns ON relay used by Thc
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_TurnON_Thc (void)
{    
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);
    return RE_OK;
}

/**
 * @Brief RE_TurnOFF_Thc
 * this function turns OFF relay used by Thc
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_TurnOFF_Thc (void)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/