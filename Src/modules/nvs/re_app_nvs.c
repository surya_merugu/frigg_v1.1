/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_nvs.c
  * Origin Date           :   10/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "nvs/re_app_nvs.h"

#define EEPROM_ADDRESS 0xA0

SystemConfig_t SystemConfig;
uint8_t * AddressOfStruct = (uint8_t*)(&SystemConfig);
uint8_t SysConfig_Buffer[sizeof(SystemConfig_t)];
bool Tx_SysConfigureData_Flag = false;
bool write_g2g_to_nvs_flag;

RE_StatusTypeDef RE_WriteDatatoNVS(void)
{
    if(HAL_I2C_Mem_Write (&hi2c1_t, EEPROM_ADDRESS,  0, 0xFFFF, AddressOfStruct, sizeof(SystemConfig), 1000) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_ReadDatafromNVS(void)
{
    HAL_Delay(10);
    uint16_t LenOfStruct = sizeof(SystemConfig);
    if(HAL_I2C_Mem_Read (&hi2c1_t, EEPROM_ADDRESS, 0, 0xFFFF, SysConfig_Buffer, LenOfStruct, 1000) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    SystemConfig.odo[0]            = SysConfig_Buffer[0];
    SystemConfig.odo[1]            = SysConfig_Buffer[1];
    SystemConfig.odo[2]            = SysConfig_Buffer[2];
    SystemConfig.odo[3]            = SysConfig_Buffer[3];
    ODO                            = (SystemConfig.odo[0] << 24) | (SystemConfig.odo[1] << 16) |\
                                     (SystemConfig.odo[2] << 8) | SystemConfig.odo[3];
    SystemConfig.key_status        = SysConfig_Buffer[4];  
    SystemConfig.G2G               = SysConfig_Buffer[5];
    SystemConfig.latch_status      = SysConfig_Buffer[6];
    SystemConfig.NoofPacksDetected = SysConfig_Buffer[7];
    SystemConfig.Thc_status        = SysConfig_Buffer[8];
    return RE_OK;
}

RE_StatusTypeDef RE_WriteBytetoNVS(uint16_t MemAddr, uint8_t Data)
{
    HAL_Delay(10);
    if(HAL_I2C_Mem_Write (&hi2c1_t, EEPROM_ADDRESS,  MemAddr, 0xFFFF, &Data, 1, 1000) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_WriteOdoToNVS(uint8_t * Data)
{
    if(HAL_I2C_Mem_Write (&hi2c1_t, EEPROM_ADDRESS,  0, 0xFFFF, Data, 4, 1000) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;   
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/