/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_location.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "location/re_app_location.h"

bool tx_pack_detection_Flag = false;

/**
 * @Brief RE_Packs_Detection
 * this function checks for the pack detection
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Packs_Detection (void)
{
    uint8_t Pack1_Detected = 0, Pack2_Detected = 0, Pack3_Detected = 0, Pack4_Detected = 0;
    if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_6) == 1)
    {
        Pack1_Detected = 1 << 3;
    }
    if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_7) == 1)
    {
        Pack2_Detected = 1 << 2;
    }
    if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_8) == 1)
    {
        Pack3_Detected = 1 << 1;
    }
    if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_9) == 1)
    {
        Pack4_Detected = 1;
    }
    SystemConfig.NoofPacksDetected = (Pack4_Detected | Pack3_Detected | Pack2_Detected | Pack1_Detected);
    return RE_OK;
}

/**
 * @Brief RE_TurnON_LocationPins
 * this function turns ON the location pins
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_TurnON_LocationPins(void)
{
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9, GPIO_PIN_SET);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/