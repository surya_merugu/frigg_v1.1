/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_gear_status.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "gear_status/re_app_gear_status.h"

static uint8_t Gear_directionA = 0, Gear_directionB = 0;

/**
 * @Brief RE_Gear_Direction
 * this function checks for the gear direction
 * @Param None
 * @Retval Gear Direction
 */
uint8_t RE_Gear_Direction (void)
{
    Gear_directionA = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_6);
    Gear_directionB = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_7);
    if (Gear_directionA == 0 && Gear_directionB == 1)       //gear neutral`
    {
        return can_tx_msg[2] = 1;
    }
    else if (Gear_directionA == 1 && Gear_directionB == 0)  //gear reverse
    {
        return can_tx_msg[2] = 2;
    }
    else if (Gear_directionA == 0 && Gear_directionB == 0)  //gear drive
    {
        return can_tx_msg[2] = 3;
    }
    else
    {
        return can_tx_msg[2] = 0;
    }
}

RE_StatusTypeDef RE_Turn_ON_RevLight(void)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
    return RE_OK;
}

RE_StatusTypeDef RE_Turn_OFF_RevLight(void)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/