/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_location.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "dock_latch/re_app_dock_latch.h"

bool tx_latch_status_Flag = false;
bool latch_opened = false;

/**
 * @Brief RE_Open_DockLatch
 * this function turns ON the relay used by dock latch
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Open_DockLatch (void)
{
    latch_opened = true;
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
    return RE_OK;
}

/**
 * @Brief RE_Close_DockLatch
 * this function turns OFF the relay used by dock latch
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Close_DockLatch (void)
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/