/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   main.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main/main.h"

uint32_t pclk1;

/**
  * @Brief  The application entry point.
  * @Retval int
  */
int main(void)
{
    HAL_Init();
    RE_SystemClock_Config();
    RE_CAN1_Init();
    RE_CAN1_Filter_Config();
    RE_CAN1_Start_Interrupt();
    RE_NVS_Init();
    RE_UART2_Init();
    Ringbuf_Init();
    RE_latch_Init();
    RE_location_Init();
    RE_TurnON_LocationPins();
    RE_key_Init();
    RE_thc_Init();
    RE_gear_Init();
    RE_speed_Init();
    RE_TIMER3_Init();
    RE_TIMER2_Init();
    RE_vb_detection_Init();
    RE_TurnON_Thc();
    RE_Load_System_Status();
    RE_ReadDatafromNVS();
    RE_Transmit_key_Detection();
    if(HAL_TIM_Base_Start_IT(&htim2_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    if(HAL_TIM_Base_Start_IT(&htim3_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    while (1)
    {
        RE_Idle_State_Handler();
    }
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/