/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_can.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Includes*/

#include "can/re_app_can.h"

uint8_t can_tx_msg[3];

/**
 * @Brief RE_Transmit_Frequent_Data
 * This function transmits gear direction, speed to front MCU 
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Transmit_Frequent_Data(void)
{     
    uint32_t TxMailbox;
    uint8_t tx_msg[5];   //0:MsgId, 1:speed, 2: Latch status 3: Thc Status 4: Backup battery voltage 
    tx_msg[0]           = 0x02;             
    tx_msg[1]           = can_tx_msg[1];
    tx_msg[2]           = SystemConfig.latch_status;
    tx_msg[3]           = SystemConfig.Thc_status;
    tx_msg[4]           = Backup_BatVoltage;
    TxHeader_t.DLC      = 5;       
    TxHeader_t.ExtId    = 0x7D0;
    TxHeader_t.IDE      = CAN_ID_EXT; 
    TxHeader_t.RTR      = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, tx_msg, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
 * @Brief RE_Transmit_Latch_Status
 * this function transmits latch status to front MCU 
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Transmit_Latch_Status(void)
{
    uint8_t tx_msg[4];     //0:MsgId, 1:latch status, 2: NoofPacksDetected 3: Thc Status
    tx_msg[0] = 0x04;
    tx_msg[1] = SystemConfig.latch_status;
    tx_msg[2] = SystemConfig.NoofPacksDetected;
    tx_msg[3] = SystemConfig.Thc_status;
    uint32_t TxMailbox; 
    TxHeader_t.DLC          = 4;       
    TxHeader_t.ExtId        = 0x7D0;
    TxHeader_t.IDE          = CAN_ID_EXT;
    TxHeader_t.RTR          = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, tx_msg, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    return RE_OK;
}

/**
 * @Brief RE_Transmit_key_Detection
 * this function transmits key status to front MCU 
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Transmit_key_Detection(void)
{
    uint8_t tx_msg[8];       //0:MsgId, 1:key status, 2: latch_status 3: Thc Status [4-7]: ODO
    uint32_t TxMailbox;
    tx_msg[0]           = 0x01;
    tx_msg[1]           = SystemConfig.key_status;
    tx_msg[2]           = SystemConfig.latch_status;
    tx_msg[3]           = SystemConfig.Thc_status;
    tx_msg[4]           = SystemConfig.odo[0];
    tx_msg[5]           = SystemConfig.odo[1];
    tx_msg[6]           = SystemConfig.odo[2];
    tx_msg[7]           = SystemConfig.odo[3];     
    TxHeader_t.DLC      = 8;       
    TxHeader_t.ExtId    = 0x7D0;
    TxHeader_t.IDE      = CAN_ID_EXT;
    TxHeader_t.RTR      = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, tx_msg, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    return RE_OK;
}

/**
 * @Brief RE_Transmit_odo
 * this function transmits odo to front MCU 
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Transmit_odo(void)
{
    uint8_t tx_msg[5];     //0:MsgId, [1-4]: ODO
    uint32_t TxMailbox; 
    tx_msg[0]           = 0x03;
    tx_msg[1]           = SystemConfig.odo[0];
    tx_msg[2]           = SystemConfig.odo[1];
    tx_msg[3]           = SystemConfig.odo[2];
    tx_msg[4]           = SystemConfig.odo[3];
    TxHeader_t.DLC      = 5;       
    TxHeader_t.ExtId    = 0x7D0;
    TxHeader_t.IDE      = CAN_ID_EXT;
    TxHeader_t.RTR      = CAN_RTR_DATA;
    if(HAL_CAN_AddTxMessage(&hcan1_t, &TxHeader_t, tx_msg, &TxMailbox) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    } 
    return RE_OK;
}

RE_StatusTypeDef RE_Transmit_SysConfigureData(void)
{
    RE_ReadDatafromNVS();
    RE_Transmit_key_Detection();
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/