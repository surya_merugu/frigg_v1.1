/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_vb_detection_init.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_vb_detection_init.h"

/**
  * @Brief RE_vb_detection_Init
  * This function configures the GPIO pins used by backup battery for voltage detection
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_vb_detection_Init (void)
{
    GPIO_InitTypeDef vb_detect, vb_detect_ctrl;
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
     /*
     * PC3   ------> 12v detect(VB1)
     * PB4   ------> 10.8v detect(VB2)
     * PB5   ------> 9.8v detect(VB3)
     * PB13  ------> VB detection Control
     */      
     vb_detect.Pin        = GPIO_PIN_3;
     vb_detect.Mode       = GPIO_MODE_INPUT;
     vb_detect.Speed      = GPIO_SPEED_FAST;
     vb_detect.Pull       = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOC, &vb_detect);

     vb_detect.Pin         = GPIO_PIN_4 | GPIO_PIN_5;
     vb_detect.Mode        = GPIO_MODE_INPUT;
     vb_detect.Speed       = GPIO_SPEED_FAST;
     vb_detect.Pull        = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOB, &vb_detect);
     
     vb_detect_ctrl.Pin      = GPIO_PIN_13;
     vb_detect_ctrl.Mode     = GPIO_MODE_OUTPUT_PP;
     vb_detect_ctrl.Speed    = GPIO_SPEED_FAST;
     vb_detect_ctrl.Pull     = GPIO_PULLDOWN;
     HAL_GPIO_Init(GPIOB, &vb_detect_ctrl);    
     return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/