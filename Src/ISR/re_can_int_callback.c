/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_can_int_callback.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_can_int_callback.h"
   
/**
  ==============================================================================
                          ##### Callback functions #####
  ==============================================================================
    [..]
    This subsection provides the following callback functions:
      ( ) HAL_CAN_TxMailbox0CompleteCallback
      ( ) HAL_CAN_TxMailbox1CompleteCallback
      ( ) HAL_CAN_TxMailbox2CompleteCallback
      ( ) HAL_CAN_TxMailbox0AbortCallback
      ( ) HAL_CAN_TxMailbox1AbortCallback
      ( ) HAL_CAN_TxMailbox2AbortCallback
      (+) HAL_CAN_RxFifo0MsgPendingCallback
      ( ) HAL_CAN_RxFifo0FullCallback
      ( ) HAL_CAN_RxFifo1MsgPendingCallback
      ( ) HAL_CAN_RxFifo1FullCallback
      ( ) HAL_CAN_SleepCallback
      ( ) HAL_CAN_WakeUpFromRxMsgCallback
      ( ) HAL_CAN_ErrorCallback
  ==============================================================================
*/

/**
  * @Brief HAL_CAN_RxFifo0MsgPendingCallback
  * This function handles the messages in CAN FIFO0
  * @Param hcan: CAN_HandleTypeDef
  * @Retval None
  */
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
    uint8_t rcvd_msg[8];
    if(HAL_CAN_GetRxMessage(&hcan1_t, CAN_RX_FIFO0, &RxHeader_t, rcvd_msg) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    if(RxHeader_t.ExtId == 0xCB)
    {
        switch(rcvd_msg[0])
        {
//            case 0x11:
//              if(rcvd_msg[1] == 1)
//              {
//                  RE_TurnON_key();
//              }
//              else if(rcvd_msg[1] == 0)
//              {
//                  RE_TurnOFF_key();
//              }
//              break;
            case 0x12:
              RE_Open_DockLatch();
              break;
            case 0x13:
              tx_latch_status_Flag = true;
              break;
            case 0x14:
              if(rcvd_msg[1] == 0)
              {
                  RE_Turn_ON_RevLight();
              }     
              else if(rcvd_msg[1] == 1)
              {
                  RE_Turn_OFF_RevLight();
              }  
              break;
            case 0x15:
                Tx_SysConfigureData_Flag = true;
                break;
            case 0x16:
                SystemConfig.G2G = rcvd_msg[1];
                write_g2g_to_nvs_flag = true;
                break;
            default:
              break;
        }          
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/