/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_gpio_int_callback.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_gpio_int_callback.h"

/**
 * @Brief GPIO External interrupt Callback
 * This function handles GPIO interrupts
 * @Param  GPIO Pin Number
 * @Retval None
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if(GPIO_Pin == GPIO_PIN_4)
    {
        pulse_counter++;
    }
    else if(GPIO_Pin == GPIO_PIN_0)
    {
        SystemConfig.latch_status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0);
        RE_Packs_Detection();
        tx_latch_status_Flag = true;
    }
    else if(GPIO_Pin == GPIO_PIN_1)
    {
        SystemConfig.key_status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1);
        Write_KeyState_Flag = true;
        tx_key_status_Flag = true;
    }
    else if(GPIO_Pin == GPIO_PIN_2)
    {
        SystemConfig.Thc_status = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2);
        tx_thc_Flag = true;
    }
    else 
    {
        __NOP();
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/