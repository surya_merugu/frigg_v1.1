/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_timer_init_callback.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_timer_int_callback.h"

bool t500ms_Elapsed = false;

/**
  * @Brief HAL_TIM_PeriodElapsedCallback
  * This function handles Timer interrupts
  * @Param htim: TIM_HandleTypeDef
  * @Retval None
  */
void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim)
{ 
    if(htim -> Instance == TIM3)
    {
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_12);
        t500ms_Elapsed = true;
    }
    else if(htim -> Instance == TIM2)
    {
        RpmCnt = pulse_counter;
        pulse_counter = 0;
    }
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/