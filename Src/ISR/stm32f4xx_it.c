/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   stm32f4xx_it.c
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main/main.h"
#include "stm32f4xx_it.h"

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @Brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{

}

/**
  * @Brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  while (1)
  {

  }
}

/**
  * @Brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  while (1)
  {

  }
}

/**
  * @Brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  while (1)
  {

  }
}

/**
  * @Brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  while (1)
  {

  }
}

/**
  * @Brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{

}

/**
  * @Brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{

}

/**
  * @Brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{

}

/**
  * @Brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  HAL_IncTick();
}

/**
  * @Brief This function handles CAN1 TX interrupts.
  */
void CAN1_TX_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @Brief This function handles CAN1 RX0 interrupts.
  */
void CAN1_RX0_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @Brief This function handles CAN1 RX1 interrupt.
  */
void CAN1_RX1_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @Brief This function handles CAN1 SCE interrupt.
  */
void CAN1_SCE_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
 * @Brief This function handles Timer interrupts
 */
void TIM3_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&htim3_t);
}

/**
 * @Brief This function handles Timer interrupts
 */
void TIM2_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&htim2_t);
}

/**
 * @Brief This function handles GPIO Pin0 interrupts
 */
void EXTI0_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

/**
 * @Brief This function handles GPIO Pin1 interrupts
 */
void EXTI1_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

/**
 * @Brief This function handles GPIO Pin2 interrupts
 */
void EXTI2_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

/**
 * @Brief This function handles GPIO Pin4 interrupts
 */
void EXTI4_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
}

/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/