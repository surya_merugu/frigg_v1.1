/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_nvs.h
  * Origin Date           :   10/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_NVS_H
#define _RE_APP_NVS_H

#include "stm32f4xx_hal.h"
#include "re_std_def.h"
#include "stm32f4xx_it.h"
#include "re_i2c_init.h"
#include "can/re_app_can.h"

typedef struct __attribute__((packed))
{
    uint8_t odo[4];
    uint8_t key_status;
    uint8_t G2G;
    uint8_t latch_status;
    uint8_t NoofPacksDetected;
    uint8_t Thc_status;
}SystemConfig_t;

extern SystemConfig_t SystemConfig;
extern bool Tx_SysConfigureData_Flag;
extern bool write_g2g_to_nvs_flag;

RE_StatusTypeDef RE_WriteDatatoNVS(void);
RE_StatusTypeDef RE_ReadDatafromNVS(void);
RE_StatusTypeDef RE_WriteBytetoNVS(uint16_t MemAddr, uint8_t Data);
RE_StatusTypeDef RE_WriteOdoToNVS(uint8_t * Data);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/