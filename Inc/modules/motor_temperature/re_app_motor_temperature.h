/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_motor_temperature.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_APP_MOTOR_TEMPERATURE_H
#define _RE_APP_MOTOR_TEMPERATURE_H

#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "re_std_def.h"
#include "stm32f4xx_it.h"
#include "can/re_app_can.h"

extern bool tx_thc_Flag;

uint8_t RE_Read_Thc_Status (void);
RE_StatusTypeDef RE_TurnON_Thc (void);
RE_StatusTypeDef RE_TurnOFF_Thc (void);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/