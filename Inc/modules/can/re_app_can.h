/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   re_app_can.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_APP_CAN_H
#define __RE_APP_CAN_H

#include "string.h"
#include "re_can_init.h"
#include "location/re_app_location.h"
#include "dock_latch/re_app_dock_latch.h"
#include "re_gpio_int_callback.h"
#include "key_position/re_app_key_position.h"
#include "backup_battery/re_app_backup_battery.h"

extern uint8_t can_tx_msg[3];

RE_StatusTypeDef RE_Transmit_Frequent_Data(void);
RE_StatusTypeDef RE_Transmit_Pack_Detection(void);
RE_StatusTypeDef RE_Transmit_Latch_Status(void);
RE_StatusTypeDef RE_Transmit_key_Detection(void);
RE_StatusTypeDef RE_Transmit_odo(void);
RE_StatusTypeDef RE_Transmit_SysConfigureData(void);

#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/