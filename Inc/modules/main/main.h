/**
  *****************************************************************************
  * Title                 :   Rear MCU
  * Filename              :   main.h
  * Origin Date           :   08/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f4xx_hal.h"
#include "re_std_def.h"
#include "re_sys_clk_config.h"
#include "re_can_init.h"
#include "re_i2c_init.h"
#include "re_uart_init.h"
#include "re_timer_init.h"
#include "re_latch_init.h"
#include "re_location_init.h"
#include "re_key_init.h"
#include "re_speed_init.h"
#include "re_thc_init.h"
#include "re_gear_init.h"
#include "re_vb_detection_init.h"
#include "idle_state/re_app_idle_state.h"
#include "uart/re_app_UART_Ring.h"
#include "can/re_app_can.h"
#include "nvs/re_app_nvs.h"

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/